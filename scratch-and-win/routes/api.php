<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Api'],function(){
    Route::post('/user/login', 'MobileUsersController@login');
    Route::group(['middleware'=>'authUser'],function() {
        Route::get('/user/get-points', 'MobileUsersController@getPoints');
        Route::post('/user/add-points', 'MobileUsersController@addPoints');
        Route::post('/user/upload-barcode-image', 'MobileUsersController@uploadBarcodeImage');
        Route::post('/user/update-barcode-image', 'MobileUsersController@updateBarcodeImage');
    });
});
