<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Http\Request;

class ApiController extends BaseController {
    public function sendResponse($status,$message,$data){
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function getUser($request) {
        // dd($request->header('Authorization'));
        $userId = $this->UserSession->where('session', '=', $request->header('Authorization'))->first()->mobile_user_id;
        $user = $this->MobileUser->whereId($userId)->first();
// dd($userId);
        return $user;
    }
}
