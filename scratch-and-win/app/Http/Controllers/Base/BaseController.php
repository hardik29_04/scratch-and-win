<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\MobileUsers;
use App\Models\WithdrawMoney;
use App\Models\UserSession;

class BaseController extends Controller
{
    public function __construct(){
        $this->User = new User;
        $this->MobileUser = new MobileUsers;
        $this->WithdrawMoney = new WithdrawMoney;
        $this->UserSession = new UserSession;
    }
}
