<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Base\ApiController;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use Carbon;
use File;
class MobileUsersController extends ApiController {
    public function login(Request $request){

        $validate = Validator::make($request->all(), [
            'ip_address' => 'required|ip'
        ]);
        
        if ($validate->fails()) {
            return $this->sendResponse(422, 'Validation Error', $validate->errors()->first());
        }

        $mobileUser = $this->MobileUser->whereIpAddress($request->ip_address)->first();

        if (empty($mobileUser)) {


            $mobileUser = $this->MobileUser;
            $mobileUser->ip_address = $request->ip_address;
            $mobileUser->points = 0;
            $mobileUser->barcode_image = null;
            $mobileUser->save();

            $userSession = $this->UserSession;
            $userSession->session = Str::random(80);
            $userSession->mobile_user_id = $mobileUser->id;
            $userSession->save();

            $data['token'] = $userSession->session;

            return $this->sendResponse(200, 'Signup Successful', $data);
        } else {

            $this->UserSession->where('mobile_user_id',$mobileUser->id)->delete();

            $userSession = $this->UserSession;
            $userSession->session = Str::random(80);
            $userSession->mobile_user_id = $mobileUser->id;
            $userSession->save();

            $data['token'] = $userSession->session;

            return $this->sendResponse(200, 'Login Successful', $data);
        }

    }


    public function addPoints(Request $request) {
        $userId = $this->getUser($request)->id;
        $user = $this->MobileUser->whereId($userId)->first();

        $totalPoints['points'] = $user->points + $request->points;
        $this->MobileUser->whereId($userId)->update(['points' => $totalPoints['points']]);

        if ($totalPoints['points'] > 10000) {
            $withdrawMoney = $this->WithdrawMoney;
            $withdrawMoney->user_id = $userId;
            $withdrawMoney->status = 0;
            $withdrawMoney->save();

            return $this->sendResponse(200, 'Withdraw Request added', $totalPoints);
        }
        return $this->sendResponse(200, 'Points Successfully added', $totalPoints);
    }


    public function getPoints(Request $request) {
        $userId = $this->getUser($request)->id;
        // dd($userId);
        $user = $this->MobileUser->whereId($userId)->first();
        $totalPoints['points'] = $user->points;
        // dd($totalPoints);

        return $this->sendResponse(200, 'Points Successfully get', $totalPoints);
    }

    public function uploadBarcodeImage(Request $request) {

        $validate = Validator::make($request->all(), [
            'barcode_image' => 'required|image|mimes:jpg,jpeg,png|max:3000'
        ]);
        
        if ($validate->fails()) {
            return $this->sendResponse(422, 'Validation Error', $validate->errors());
        }

        $userId = $this->getUser($request)->id;
        // dd($userId);
        $user = $this->MobileUser->whereId($userId)->first();
        

        $path = public_path('upload/mobile_users/barcode_images');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        $image = $request->barcode_image;

        $path = public_path() . 'upload/mobile_users/barcode_images';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $fileName = 'upload/mobile_users/barcode_images/' . Carbon\Carbon::now()->format('YmdHisu') . '.' . $image->getClientOriginalExtension();
        $image->move('public/upload/mobile_users/barcode_images/', $fileName);


        $user->barcode_image = $fileName;
        $user->save();


        return $this->sendResponse(200, 'Image Successfully added', $user);
    }

    public function updateBarcodeImage(Request $request) {

        $validate = Validator::make($request->all(), [
            'barcode_image' => 'required|image|mimes:jpg,jpeg,png|max:3000'
        ]);
        
        if ($validate->fails()) {
            return $this->sendResponse(422, 'Validation Error', $validate->errors());
        }

        $userId = $this->getUser($request)->id;
        // dd($userId);
        $user = $this->MobileUser->whereId($userId)->first();
        // dd(file_exists(public_path() . '/' . $user->barcode_image));

        if (file_exists(public_path() . '/' . $user->barcode_image)) {
            File::delete(public_path() . '/' . $user->barcode_image);
        }



        $path = public_path('upload/mobile_users/barcode_images');
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }


        $image = $request->barcode_image;

        $path = public_path() . 'upload/mobile_users/barcode_images';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $fileName = 'public/upload/mobile_users/barcode_images/' . Carbon\Carbon::now()->format('YmdHisu') . '.' . $image->getClientOriginalExtension();
        $image->move('public/upload/mobile_users/barcode_images/', $fileName);


        $user->barcode_image = $fileName;
        $user->save();


        return $this->sendResponse(200, 'Image Successfully added', $user);
    }

}
