<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Base\AdminController;
use Illuminate\Http\Request;
use View;
use Carbon;

class MobileUsersController extends AdminController {
    public function index(Request $request) {
        if ($request->ajax()) {
            // $search = $request->search;
            // $category = $request->category;
            // $condition = $request->condition;
            // $brand = $request->brand;
            $startDate = $request->start_date;
            $endDate = $request->end_date;

            $mobileUsers = $this->MobileUser->query();
            // if ($search) {
            //     $products = $products->where(function ($query) use ($search) {
            //         $query->where('name', 'LIKE', '%' . $search . '%')
            //         ->orWhere('email', 'LIKE', '%' . $search . '%')
            //         ->orWhere('phone_number', 'LIKE', '%' . $search . '%');
            //     });
            // }
            // if ($category) {
            //     $products = $products->where('category', '=', $category);
            // }
            // if ($condition) {
            //     $products = $products->where('condition', '=', $condition);
            // }
            // if ($brand) {
            //     $products = $products->where('brand', 'LIKE', '%' . $brand . '%');
            // }
            if (!empty($startDate) && !empty($endDate)) {
                $mobileUsers = $mobileUsers->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
            $mobileUsers = $mobileUsers->paginate(10);


            return response()->json(
                View::make('admin.mobile_users.data',compact('mobileUsers'))->render()
            );
        }
        return view('admin.mobile_users.index');
    }
}
