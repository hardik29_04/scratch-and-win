<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Base\AdminController;
use Illuminate\Http\Request;
use View;
use Carbon;
use Response;
use Exception;

class WithdrawMoneyController extends AdminController {
    public function index(Request $request) {
        if ($request->ajax()) {
            $startDate = $request->start_date;
            $endDate = $request->end_date;

            $withdrawMoney = $this->WithdrawMoney->whereStatus(0)
                ->whereHas('getMobileUserData', function($query) {
                    $query->where('barcode_image' , '!=', null)
                    ->where('points', '>', 10000);
                })
                ->orderBy('created_at', 'DESC');


            if (!empty($startDate) && !empty($endDate)) {
                $withdrawMoney = $withdrawMoney->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }


            $withdrawMoney = $withdrawMoney->paginate(10);


            return response()->json(
                View::make('admin.withdraw_money.data',compact('withdrawMoney'))->render()
            );
        }
        return view('admin.withdraw_money.index');
    }

     public function approveRequest($id) {
        try {
            $id = decrypt($id);
        } catch (Exception $e) {
            return Response::json([
                'success' => 2,
            ], 400);
        }


        $withdrawMoney = $this->WithdrawMoney->whereId($id)->first();
        $withdrawMoney->status = 1;
        $withdrawMoney->save();


        $user = $this->MobileUser->whereId($withdrawMoney->user_id)->first();

        $user->points -= 10000; 
        $user->save();

        
        return Response::json([
            'success' => 1,
        ], 200);

    }
}
