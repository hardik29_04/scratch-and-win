<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Base\AdminController;
use Illuminate\Http\Request;

class UserController extends AdminController {
    public function index(Request $request) {
        if($request->ajax()){
            $search = $request->searchtext;
            $user = User::query();
            if($search){
                $user = $user->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%'.$search.'%')
                    ->orWhere('email', 'LIKE', '%'.$search.'%')
                    ->orWhere('mobile_number', 'LIKE', '%'.$search.'%');
                });
            }
            $user = $user->paginate(10);
            return response()->json(
                View::make('admin.user.data',compact('user'))
                ->render()
            );
        }
        return view('admin.user.index');
    }
}
