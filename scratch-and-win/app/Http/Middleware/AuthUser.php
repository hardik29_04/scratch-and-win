<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\UserSession;

class AuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('Authorization');
        $userSession = UserSession::where('session',$token)->first();
        if (!$userSession) {
            return response()->json([
                'status' => 401,
                'message' => 'Unauthorized',
                'data' => []
            ],401);
        } else {
            return $next($request);
        }
    }
}
