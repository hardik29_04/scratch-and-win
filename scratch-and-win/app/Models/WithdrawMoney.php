<?php

namespace App\Models;
use App\Models\Base\Base;

class WithdrawMoney extends Base {
    public function getMobileUserData(){
        return $this->belongsTo('App\Models\MobileUsers', 'user_id', 'id');
    }
}
