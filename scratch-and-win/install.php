<?php

// Set variables for our request
$shop = $_GET['shop'];
$api_key = "73b62fdbc649e07c7312afeec8c55eaf";
$scopes = "read_products";
$redirect_uri = "https://sellwithroh-roh.com/generate_token.php";

// Build install/approval URL to redirect to
$install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . $api_key . "&scope=" . $scopes . "&redirect_uri=" . urlencode($redirect_uri);

// Redirect
header("Location: " . $install_url);
die();