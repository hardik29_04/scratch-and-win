@extends('admin_template.main') 

@section('title')
    Product - Sell With Roh Roh
@endsection

@section('content')  
<div class="content-header">
      <div class="container-fluid">

<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-hover product-details-table">
        <tr>
            <th>
                Name
            </th>
            <td>
                {{ $product->name  }}
            </td>
        </tr>
        <tr>
            <th>
                Email
            </th>
            <td>
                {{ $product->email  }}
            </td>
        </tr>
        <tr>
            <th>
                Phone Number
            </th>
            <td>
                {{ $product->phone_number  }}
            </td>
        </tr>
        <tr>
            <th>
                Brand
            </th>
            <td>
                {{ $product->brand  }}
            </td>
        </tr>
        <tr>
            <th>
                Category
            </th>
            <td>
                @if ($product->category == 1)
                    Bag
                @elseif ($product->category == 2)
                    SDhoes
                @elseif ($product->category == 3)
                    Clothing
                @else
                    Accessories
                @endif
            </td>
        </tr>
        <tr>
            <th>
                Model Code
            </th>
            <td>
                {{ $product->model_code  }}
            </td>
        </tr>
        <tr>
            <th>
                Condition
            </th>
            <td>
                @if ($product->condition == 1)
                    Pristine - Unused
                @elseif ($product->condition == 2)
                    Excellent condition - used lightly
                @elseif ($product->condition == 3)
                   Very good condition – used but well maintained
                @else
                    Good condition – well used but well maintained
                @endif
            </td>
        </tr>
       <tr>
            <th>
                Box
            </th>
            <td>
                {{ $product->box == 0 ? 'No' : 'Yes'  }}
            </td>
        </tr>
        <tr>
            <th>
                Additional Info
            </th>
            <td class="w-75">
                {{ $product->additional_info  }}
            </td>
        </tr>
    </table>
  </div>
</div>
<div class="row">
    <div class="main-product-images">
            <div class="col-md-3 d-inline-flex text-center">
                <figure itemprop="associatedMedia" class="main-product-figure" itemscope itemtype="http://schema.org/ImageObject">
                    <?php 
                        $data = getimagesize(URL::to('/') . '/' . $product->front);
                        $width = $data[0];
                        $height = $data[1];
                    ?>
                    <a href="{{ URL::to('/') . '/' . $product->front }}"  class="main-product-a" data-width="{{ $width }}" data-height="{{ $height }}" itemprop="contentUrl">
                        <img src="{{ URL::to('/') . '/' . $product->front }}" class="feature-row__image w-75">            
                    </a>
                </figure>
            </div>
            @if (!empty($product->front_side))
                <div class="col-md-3 d-inline-flex text-center">
                    <figure itemprop="associatedMedia" class="main-product-figure" itemscope itemtype="http://schema.org/ImageObject">
                        <?php 
                            $data = getimagesize(URL::to('/') . '/' . $product->front_side);
                            $width = $data[0];
                            $height = $data[1];
                        ?>
                        <a href="{{ URL::to('/') . '/' . $product->front_side }}"  class="main-product-a" data-width="{{ $width }}" data-height="{{ $height }}" itemprop="contentUrl">
                            <img src="{{ URL::to('/') . '/' . $product->front_side }}" class="feature-row__image w-75">            
                        </a>
                    </figure>
                </div>
            @endif
            @if (!empty($product->side))
                <div class="col-md-3 d-inline-flex text-center">
                    <figure itemprop="associatedMedia" class="main-product-figure" itemscope itemtype="http://schema.org/ImageObject">
                        <?php 
                            $data = getimagesize(URL::to('/') . '/' . $product->side);
                            $width = $data[0];
                            $height = $data[1];
                        ?>
                        <a href="{{ URL::to('/') . '/' . $product->side }}"  class="main-product-a" data-width="{{ $width }}" data-height="{{ $height }}" itemprop="contentUrl">
                            <img src="{{ URL::to('/') . '/' . $product->side }}" class="feature-row__image w-75">            
                        </a>
                    </figure>
                </div>
            @endif
            @if (!empty($product->back))
                <div class="col-md-3 d-inline-flex text-center">
                    <figure itemprop="associatedMedia" class="main-product-figure" itemscope itemtype="http://schema.org/ImageObject">
                        <?php 
                            $data = getimagesize(URL::to('/') . '/' . $product->back);
                            $width = $data[0];
                            $height = $data[1];
                        ?>
                        <a href="{{ URL::to('/') . '/' . $product->back }}"  class="main-product-a" data-width="{{ $width }}" data-height="{{ $height }}" itemprop="contentUrl">
                            <img src="{{ URL::to('/') . '/' . $product->back }}" class="feature-row__image w-75">            
                        </a>
                    </figure>
                </div>
            @endif
            @if (!empty($product->bottom))
                <div class="col-md-3 d-inline-flex text-center">
                    <figure itemprop="associatedMedia" class="main-product-figure" itemscope itemtype="http://schema.org/ImageObject">
                        <?php 
                            $data = getimagesize(URL::to('/') . '/' . $product->bottom);
                            $width = $data[0];
                            $height = $data[1];
                        ?>
                        <a href="{{ URL::to('/') . '/' . $product->bottom }}"  class="main-product-a" data-width="{{ $width }}" data-height="{{ $height }}" itemprop="contentUrl">
                            <img src="{{ URL::to('/') . '/' . $product->bottom }}" class="feature-row__image w-75">            
                        </a>
                    </figure>
                </div>
            @endif
            @if (!empty($product->interior))
                <div class="col-md-3 d-inline-flex text-center">
                    <figure itemprop="associatedMedia" class="main-product-figure" itemscope itemtype="http://schema.org/ImageObject">
                        <?php 
                            $data = getimagesize(URL::to('/') . '/' . $product->interior);
                            $width = $data[0];
                            $height = $data[1];
                        ?>
                        <a href="{{ URL::to('/') . '/' . $product->interior }}"  class="main-product-a" data-width="{{ $width }}" data-height="{{ $height }}" itemprop="contentUrl">
                            <img src="{{ URL::to('/') . '/' . $product->interior }}" class="feature-row__image w-75">            
                        </a>
                    </figure>
                </div>
            @endif
        
    </div>
</div>

@endsection

  <!-- Some spacing 😉 -->
<div class="spacer"></div>


<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
  <!-- Background of PhotoSwipe. 
           It's a separate element as animating opacity is faster than rgba(). -->
  <div class="pswp__bg"></div>
  <!-- Slides wrapper with overflow:hidden. -->
  <div class="pswp__scroll-wrap">
    <!-- Container that holds slides. 
              PhotoSwipe keeps only 3 of them in the DOM to save memory.
              Don't modify these 3 pswp__item elements, data is added later on. -->
    <div class="pswp__container">
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
      <div class="pswp__item"></div>
    </div>
    <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
    <div class="pswp__ui pswp__ui--hidden">
      <div class="pswp__top-bar">
        <!--  Controls are self-explanatory. Order can be changed. -->
        <div class="pswp__counter"></div>
        <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
        <button class="pswp__button pswp__button--share" title="Share"></button>
        <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
        <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
        <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
        <!-- element will get class pswp__preloader--active when preloader is running -->
        <div class="pswp__preloader">
          <div class="pswp__preloader__icn">
            <div class="pswp__preloader__cut">
              <div class="pswp__preloader__donut"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
        <div class="pswp__share-tooltip"></div>
      </div>
      <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
      </button>
      <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
      </button>
      <div class="pswp__caption">
        <div class="pswp__caption__center"></div>
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.min.css" integrity="sha512-yxWNfGm+7EK+hqP2CMJ13hsUNCQfHmOuCuLmOq2+uv/AVQtFAjlAJO8bHzpYGQnBghULqnPuY8NEr7f5exR3Qw==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/default-skin/default-skin.min.css" integrity="sha512-Rck8F2HFBjAQpszOB9Qy+NVLeIy4vUOMB7xrp46edxB3KXs2RxXRguHfrJqNK+vJ+CkfvcGqAKMJTyWYBiBsGA==" crossorigin="anonymous" />

@section('js')


<script defer src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe.min.js" integrity="sha512-2R4VJGamBudpzC1NTaSkusXP7QkiUYvEKhpJAxeVCqLDsgW4OqtzorZGpulE3eEA7p++U0ZYmqBwO3m+R2hRjA==" crossorigin="anonymous"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.3/photoswipe-ui-default.min.js" integrity="sha512-SxO0cwfxj/QhgX1SgpmUr0U2l5304ezGVhc0AO2YwOQ/C8O67ynyTorMKGjVv1fJnPQgjdxRz6x70MY9r0sKtQ==" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function(){
        // $('.main-product-images').slick({
        //     centerMode: true,
        //     centerPadding: '60px',
        //     slidesToShow: 3,
        //     responsive: [
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 arrows: false,
        //                 centerMode: true,
        //                 centerPadding: '40px',
        //                 slidesToShow: 3
        //             }
        //         },
        //         {
        //             breakpoint: 480,
        //             settings: {
        //                 arrows: false,
        //                 centerMode: true,
        //                 centerPadding: '40px',
        //                 slidesToShow: 1
        //             }
        //         }
        //     ]
        // });
       var container = [];

  // Loop over gallery items and push it to the array
  $('.main-product-images').find('figure').each(function() {
    var $link = $(this).find('a'),
      item = {
        src: $link.attr('href'),
        w: $link.data('width'),
        h: $link.data('height'),
        title: $link.data('caption')
      };
    container.push(item);
  });

  // Define click event on gallery item
  $('.main-product-a').click(function(event) {

    // Prevent location change
    event.preventDefault();
console.log($(this).parents('.for-main-product-div'));
    // Define object and gallery options
    var $pswp = $('.pswp')[0],
      options = {
        index: $(this).parents('.col-md-3').index(),
    bgOpacity: 0.9,
    captionEl: true,
    tapToClose: true,
    shareEl: true,
    fullscreenEl: true,
      };

    // Initialize PhotoSwipe
    var gallery = new PhotoSwipe($pswp, PhotoSwipeUI_Default, container, options);
    gallery.init();
  });

    });
</script>
@endsection