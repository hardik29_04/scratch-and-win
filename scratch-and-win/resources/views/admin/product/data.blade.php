<table width="100%" class="table table-bordered table-hover product-details-table">
        <thead>
        <tr>
            <th>Sr. No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Front</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse($products as $row)
                <tr>
                    <td>{{ ($products->currentpage()-1) * $products->perpage() + $loop->index + 1 }}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->phone_number}}</td>
                    <td>{{$row->brand}}</td>
                    <td>
                        
                        
                            @if ($row->category == 1)
                                Bag
                            @elseif ($row->category == 2)
                                SDhoes
                            @elseif ($row->category == 3)
                                Clothing
                            @else
                                Accessories
                            @endif
                    
                    </td>
                    <td class="front-image-td">
                        <img src="{{ URL::to('/' . $row->front) }}" class="front-image"/>
                    </td>
                    <td>
                        {{ \Carbon\Carbon::parse($row->created_at)->format('d-M-Y') }}
                    </td>
                    <td>
                        <a href="{{URL::to('product/view/' . encrypt($row->id))}}" class="btn btn-primary">View</a>
                        <a  class="btn btn-danger delete-record" data-id={{  encrypt($row->id) }}>Delete</a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9" class="text-center">No Record Available</td>
                </tr>
            @endforelse
        </tbody>
    </table>
<div class="">{!! $products->links() !!}</div>