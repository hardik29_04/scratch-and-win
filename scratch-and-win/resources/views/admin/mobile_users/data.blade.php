<table width="100%" class="table table-bordered table-hover mobile-user-details-table">
        <thead>
        <tr>
            <th>Sr. No</th>
            <th>IP Address</th>
            <th>Points</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>
            @forelse($mobileUsers as $row)
                <tr>
                    <td>{{ ($mobileUsers->currentpage()-1) * $mobileUsers->perpage() + $loop->index + 1 }}</td>
                    <td>{{ $row->ip_address }}</td>
                    <td>{{ $row->points }}</td>
                    <td>{{ \Carbon\Carbon::parse($row->created_at)->format('d-M-Y') }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="6" class="text-center">No Record Available</td>
                </tr>
            @endforelse
        </tbody>
    </table>
<div class="">{!! $mobileUsers->links() !!}</div>