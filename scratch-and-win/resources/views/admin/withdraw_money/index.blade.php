@extends('admin_template.main') 

@section('title')
    Withdraw Money
@endsection

@section('content')  
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Withdraw Money</h1>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('message'))
            <div class="alert alert-success" role="alert">
                <li><strong>{!! Session::get('message') !!}</strong></li>
            </div>
        @endif
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="daterange form-control" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="withdraw-data"></div>
            </div>
        </div>
      </div>
    </section>
@endsection


@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.0.1/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
    $(document).ready(function(){
        $('.daterange').daterangepicker();
        var page = '';
        var qstring = '' ;
        var startDate = '';
        var endDate = '';
        getWithdrawMoneyData(qstring);


        $(document).on('click','.approve-request',function(e) {
            var id = $(this).data('id');
            swal("Are you sure!", {
                buttons: {
                cancel: true,
                confirm: "Confirm",
            }
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "{{ URL::to('withdraw-money/approve-request') }}" + '/' + id,
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        dataType: 'json',
                        }).done(function (data) {
                            if (data.success == 1) {
                                swal("Request successfully Approved", {
                                    icon: "success",
                                });
                                page = 1;
                                getWithdrawMoneyData(qstring);
                            } else {
                                swal("Oops, Something went wrong", {
                                    icon: "error",
                                });
                                window.location.reload;
                            }
                        }).fail(function (result) {
                    });
                }
            });
            
        });

        $(document).on('click', '.pagination li a', function (e) {
            e.preventDefault();
            page = $(this).attr('href').split('page=')[1];
            qstring = 'page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
            getWithdrawMoneyData(qstring);
        });



            
        $('.daterange').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            startDate = picker.startDate.format('YYYY-MM-DD');
            endDate = picker.endDate.format('YYYY-MM-DD');
            qstring = 'page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
            getWithdrawMoneyData(qstring);
        });

        $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            startDate = '';
            endDate = '';
            qstring = 'page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
            getWithdrawMoneyData(qstring);
        });
            
       function getWithdrawMoneyData(qstring) {
            $.ajax({
                type: 'GET',
                url : '{{ URL::to("withdraw-money?")}}' + qstring,
                dataType: 'json',
            }).done(function (data) {
                $('.withdraw-data').html(data);     
            }).fail(function () {
            });
        }
    });
</script>
@endsection
<style>
    .barcode-image {
        width: 200px
    }
    .front-image-td {
        width: 10%;
    }
     .front-image {
        width: 100px;
    }

    @media only screen and (max-width: 768px) {
 .withdraw-money-details-table {
          display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    }
}

    
</style>