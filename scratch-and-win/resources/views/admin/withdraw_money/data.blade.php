<table width="100%" class="table table-bordered table-hover withdraw-money-details-table">
        <thead>
        <tr>
            <th>Sr. No</th>
            <th>Barcode Image</th>
            <th>Points</th>
            <th>Status</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse($withdrawMoney as $row)
                <tr>
                    <td>{{ ($withdrawMoney->currentpage()-1) * $withdrawMoney->perpage() + $loop->index + 1 }}</td>
                    <td>
                        <a href="{{ $row->getMobileUserData['barcode_image'] }}">
                          <img src="{{ $row->getMobileUserData['barcode_image'] }}" class="barcode-image" />
                        </a>
                    </td>
                    <td>{{ $row->getMobileUserData['points'] }}</td>
                    <td>
                        @if ($row->status == 0)
                         Pending
                        @else
                         Approved
                        @endif
                    </td>
                    <td>{{ \Carbon\Carbon::parse($row->created_at)->format('d-M-Y H:i A') }}</td>
                    <td>
                        <a class="approve-request btn btn-primary" data-id="{{ encrypt($row->id) }}">Approve</a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">No Record Available</td>
                </tr>
            @endforelse
        </tbody>
    </table>
<div class="">{!! $withdrawMoney->links() !!}</div>