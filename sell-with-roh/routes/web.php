<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/','Auth\LoginController@login');
Route::post('/product/store','Admin\ProductController@store');
 Route::group(['namespace'=>'Admin', 'middleware'=>'auth'],function(){
    //  Route::get('/','UserController@index');
// Route::get('/','AuthController@login');
     
     Route::get('/products','ProductController@index');
     Route::get('/product/add','ProductController@create');
     
     Route::get('/product/view/{id}','ProductController@view');
     Route::get('/product/delete/{id}','ProductController@destroy');

 });

