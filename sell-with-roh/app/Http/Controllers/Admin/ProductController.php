<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Base\AdminController;
use Illuminate\Http\Request;
use Response;
use Validator;
use View;
use Carbon;
use File;
use URL;
use Exception;

class ProductController extends AdminController {
    public function index(Request $request) {
        if ($request->ajax()) {
            $search = $request->search;
            $category = $request->category;
            $condition = $request->condition;
            $brand = $request->brand;
            $startDate = $request->start_date;
            $endDate = $request->end_date;

            $products = $this->Product->query();
            if ($search) {
                $products = $products->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                    ->orWhere('email', 'LIKE', '%' . $search . '%')
                    ->orWhere('phone_number', 'LIKE', '%' . $search . '%');
                });
            }
            if ($category) {
                $products = $products->where('category', '=', $category);
            }
            if ($condition) {
                $products = $products->where('condition', '=', $condition);
            }
            if ($brand) {
                $products = $products->where('brand', 'LIKE', '%' . $brand . '%');
            }
            if (!empty($startDate) && !empty($endDate)) {
                $products = $products->whereBetween('created_at', [$startDate . ' 00:00:00', $endDate . ' 23:59:59']);
            }
            $products = $products->paginate(10);

// dd($products);
            return response()->json(
                View::make('admin.product.data',compact('products'))->render()
            );
        }
        return view('admin.product.index');
    }

    public function create() {
        return view('admin.product.create');
    }

    public function store(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:2|max:100',
            'email' => 'required|email|min:4|max:100',
            'phone_number' => 'required|numeric',
            'brand' => 'required',
            'category' => 'required|numeric',
            'model_code' => 'nullable',
            'condition' => 'required',
            'box' => 'required',
            'additional_info' => 'nullable',
            'front' => 'required|image|mimes:jpeg,png,jpg|max:5000',
            'front_side' => 'nullable|image|mimes:jpeg,png,jpg|max:5000',
            'side' => 'nullable|image|mimes:jpeg,png,jpg|max:5000',
            'back' => 'nullable|image|mimes:jpeg,png,jpg|max:5000',
            'bottom' => 'nullable|image|mimes:jpeg,png,jpg|max:5000',
            'interior' => 'nullable|image|mimes:jpeg,png,jpg|max:5000',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'success' => false,
                'errors' => $validator->errors()
            ], 422);
        }
        

        $product = $this->Product;
        $product->name = $request->name;
        $product->email = $request->email;
        $product->phone_number = $request->phone_number;
        $product->brand = $request->brand;
        $product->category = $request->category;
        $product->model_code = $request->model_code;
        $product->condition = $request->condition;
        $product->box = $request->box;
        $product->additional_info = $request->additional_info;


        $fileName = self::uploadFile($request->file('front'));
        $product->front = 'public/images/product/' . $fileName;
        
        if ($request->hasFile('front_side')) {
            $fileName = self::uploadFile($request->file('front_side'));
            $product->front_side = 'public/images/product/' . $fileName;
        }
        if ($request->hasFile('side')) {
            $fileName = self::uploadFile($request->file('side'));
            $product->side = 'public/images/product/' . $fileName;
        }
        if ($request->hasFile('back')) {
            $fileName = self::uploadFile($request->file('back'));
            $product->back = 'public/images/product/' . $fileName;
        }
        if ($request->hasFile('bottom')) {
            $fileName = self::uploadFile($request->file('bottom'));
            $product->bottom = 'public/images/product/' . $fileName;
        }
        if ($request->hasFile('interior')) {
            $fileName = self::uploadFile($request->file('interior'));
            $product->interior = 'public/images/product/' . $fileName;
        }

        if ($product->save()) {
            return Response::json([
                'success' => true,
            ], 200);
        } else {
            return Response::json([
                'success' => false,
            ], 500);
        }
    }

    public function uploadFile($image) {
       $path = public_path() . '/images/product';
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $fileName = Carbon\Carbon::now()->format('YmdHisu') . '.' . $image->getClientOriginalExtension();
        $image->move('public/images/product/', $fileName);
        return $fileName;
    }

    public function view($id) {
        try {
            $id = decrypt($id);
        } catch (Exception $e) {
            return back();
        }

        $product = $this->Product->whereId($id)->first();
        return view('admin.product.view', compact('product'));
    }

    public function destroy($id) {
        try {
            $id = decrypt($id);
        } catch (Exception $e) {
            return Response::json([
                'success' => 2,
            ], 400);
        }

        $product = $this->Product->whereId($id)->first();

        if (!empty($product->front)) {
            unlink($product->front);
        }

        if (!empty($product->front_side)) {
            unlink($product->front_side);
        }
        if (!empty($product->back)) {
            unlink($product->back);
        }
        if (!empty($product->bottom)) {
            unlink($product->bottom);
        }
        if (!empty($product->side)) {
            unlink($product->side);
        }
        if (!empty($product->interior)) {
            unlink($product->interior);
        }
        
        if ($product->delete()) {
            return Response::json([
                'success' => 1,
            ], 200);
        } else {
            return Response::json([
                'success' => 0,
            ], 500);
        }

    }
}
