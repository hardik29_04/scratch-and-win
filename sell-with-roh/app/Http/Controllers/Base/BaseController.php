<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Product;

class BaseController extends Controller
{
    public function __construct(){
        $this->User = new User;
        $this->Product = new Product;
    }
}
