<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCommentOfConditionFieldToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('products', function (Blueprint $table) {
            DB::statement("ALTER TABLE products CHANGE `condition` `condition` tinyint(4) NOT NULL comment '1=Pristine - Unused, 2=Excellent Condition - Used Lightly, 3=Very Good Condition – Used But Well Maintained, 4=Good Condition – Well Used But Well Maintained, 5=New other - Never used but slightly marked';");
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement("ALTER TABLE products CHANGE `condition` `condition` tinyint(4) NOT NULL comment '1=Pristine - Unused, 2=Excellent Condition - Used Lightly, 3=Very Good Condition – Used But Well Maintained, 4=Good Condition – Well Used But Well Maintained';");
    }
}
