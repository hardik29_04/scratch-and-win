<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->nullable(false);
            $table->string('email', 50)->nullable(false);
            $table->string('phone_number')->nullable(false);
            $table->string('brand')->nullable(false);
            $table->tinyInteger('category')->comment('1=Bag, 2=Shoes, 3=Clothing, 4=Accessories')->nullable(false);
            $table->string('model_code')->nullable();
            $table->tinyInteger('condition')->comment('1=Pristine - Unused, 2=Excellent Condition - Used Lightly, 3=Very Good Condition – Used But Well Maintained, 4=Good Condition – Well Used But Well Maintained')->nullable(false);
            $table->tinyInteger('box')->comment('0=No, 1=Yes')->nullable(false);
            $table->text('additional_info')->nullable();
            $table->string('front')->nullable(false);
            $table->string('front_side')->nullable();
            $table->string('side')->nullable();
            $table->string('back')->nullable();
            $table->string('bottom')->nullable();
            $table->string('interior')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
