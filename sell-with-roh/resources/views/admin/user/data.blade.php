<table width="100%" class="m-dt-table table table-bordered table-hover">
        <thead>
        <tr>
            <th>Sr no.(id)</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Front Photo</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse($product as $row)
                <tr>
                    <td>{{ ($product ->currentpage()-1) * $product ->perpage() + $loop->index + 1 }}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->phone_number}}</td>
                    <td>{{$row->brand}}</td>
                    <td>
                        <?php
                            if ($row->category == 1) {
                                Unused
                            } else if ($row->category == 2) {
                                Excellent
                            } else if ($row->category == 3) {
                                Very Good
                            } else ($row->category == 4) {
                                Good
                            }
                        ?>
                    </td>
                    <td>
                        <img src="{{ URL::to($row->front_photo) }}" />
                    </td>
                    <td>
                        <a href="{{URL::to('products/view/' . encrypt($row->id))}}" class="btn btn-primary">View</a>
                    </td>
                    <td>
                        <a href="{{URL::to('products/delete/' . encrypt($row->id))}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @empty
            <tr>
                <td colspan="6"><center> No data available</center></td>
            </tr>
            @endforelse
        </tbody>
    </table>
<div class="m-datatable__pager m-datatable--paging-loaded clearfix sponser-page">{!! $user->links() !!}</div>