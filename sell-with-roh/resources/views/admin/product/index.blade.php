@extends('admin_template.main') 

@section('title')
    Product - Sell With Roh Roh
@endsection

@section('content')  
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Product</h1>
          </div>
        </div>
      </div>
    </div>
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('message'))
            <div class="alert alert-success" role="alert">
                <li><strong>{!! Session::get('message') !!}</strong></li>
            </div>
        @endif
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="daterange form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="search form-control" placeholder="Search" />
                </div>
            </div>
            <div class="col-md-3">
                 <div class="form-group">
                    <select class="form-control category">
                        <option value="" selected>Select Category</option>
                        <option value="1">Bag</option>
                        <option value="2">Shoes</option>
                        <option value="3">Clothing</option>
                        <option value="4">Accessories</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                 <div class="form-group">
                    <select class="form-control condition">
                        <option value="">Select Condition</option>
                        <option value="1">Unused</option>
                        <option value="2">Excellent </option>
                        <option value="3">Very Good</option>
                        <option value="4">Good </option>
                        <option value="5">Never used but slightly marked</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
    <div class="form-group">
      <select class="form-control brand" name="brand">
          <option value="">Select Brand</option>
        <option value="Aquazzura">Aquazzura</option>
        <option value="Alexander McQueen">Alexander McQueen</option>
        <option value="Alexander Wang">Alexander Wang</option>
        <option value="Balenciaga">Balenciaga</option>
        <option value="Balmain">Balmain</option>
        <option value="Bottega Veneta">Bottega Veneta</option>
        <option value="Burberry">Burberry</option>
        <option value="Bulgari">Bulgari</option>
        <option value="Cartier">Cartier</option>
        <option value="Celine">Celine</option>
        <option value="Chanel">Chanel</option>
        <option value="Chloe">Chloe</option>
        <option value="Christian Louboutin">Christian Louboutin</option>
        <option value="Dior">Dior</option>
        <option value="Dolce & Gabbana">Dolce & Gabbana</option>
        <option value="Emilio Pucci">Emilio Pucci</option>
        <option value="Fendi">Fendi</option>
        <option value="Givenchy">Givenchy</option>
        <option value="Gucci">Gucci</option>
        <option value="Hermes">Hermes</option>
        <option value="Jacquemus">Jacquemus</option>
        <option value="Jimmy Choo">Jimmy Choo</option>
        <option value="Lanvin">Lanvin</option>
        <option value="Louis Vuitton">Louis Vuitton</option>
        <option value="Loewe">Loewe</option>
        <option value="Missoni">Missoni</option>
        <option value="Miu Miu">Miu Miu</option>
        <option value="Moschino">Moschino</option>
        <option value="Mulberry">Mulberry</option>
        <option value="Prada">Prada</option>
        <option value="Salvatore Ferragamo">Salvatore Ferragamo</option>
        <option value="Stella McCartney">Stella McCartney</option>
        <option value="Tom Ford">Tom Ford</option>
        <option value="Valentino">Valentino</option>
        <option value="Versace">Versace</option>
        <option value="Vivienne Westwood">Vivienne Westwood</option>
        <option value="YSL">YSL</option>
      </select>
      <span class="error brand"></span>
    </div>
  </div> 
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="product-data"></div>
            </div>
        </div>
      </div>
    </section>
@endsection


@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.0.1/sweetalert.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
    $(document).ready(function(){
        $('.daterange').daterangepicker();
        var page = '';
        var qstring = '' ;
        var search = '';
        var brand = '';
        var category = '';
        var condition = '';
        var startDate = '';
        var endDate = '';
        getProductData(qstring);

        $(document).on('click','.delete-record',function(e) {
            var id = $(this).data('id');
            swal("Are you sure!", {
                buttons: {
                cancel: true,
                confirm: "Confirm",
            }
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "{{ URL::to('product/delete') }}" + '/' + id,
                        type: 'GET',
                        dataType: 'json',
                        }).done(function (data) {
                            if (data.success == 1) {
                                swal("Record successfully deleted", {
                                    icon: "success",
                                });
                                console.log(data);
                                page = 1;
                                getProductData(qstring);
                            } else {
                                swal("Oops, Something went wrong", {
                                    icon: "error",
                                });
                                window.location.reload;
                            }
                        }).fail(function (result) {
                    });
                } else {
                    
                }
            });
            
        });

        $(document).on('click', '.pagination li a', function (e) {
            e.preventDefault();
            page = $(this).attr('href').split('page=')[1];
            qstring = 'condition=' + condition + '&search=' + search + '&category=' + category + '&brand=' + brand + '&page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
            getProductData(qstring);
        });

            
        $(document).on('keyup','.search',function(e){
            e.preventDefault();
            search = $(this).val();
            qstring = 'condition=' + condition + '&search=' + search + '&category=' + category + '&brand=' + brand + '&page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
            getProductData(qstring);
        });

        $(document).on('change','.category', function(e) {
            e.preventDefault();
            category = $('.category option:selected').val();
            qstring = 'condition=' + condition + '&search=' + search + '&category=' + category + '&brand=' + brand + '&page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
            getProductData(qstring);
        });

         $(document).on('change','.condition', function(e) {
            e.preventDefault();
            condition = $('.condition option:selected').val();
            qstring = 'condition=' + condition + '&search=' + search + '&category=' + category + '&brand=' + brand + '&page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
            getProductData(qstring);
        });
        $(document).on('change','.brand', function(e) {
            e.preventDefault();
            brand = $('.brand option:selected').val();
            qstring = 'condition=' + condition + '&search=' + search + '&category=' + category + '&brand=' + brand + '&page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
            getProductData(qstring);
        });
            
$('.daterange').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
      startDate = picker.startDate.format('YYYY-MM-DD');
      endDate = picker.endDate.format('YYYY-MM-DD');
      qstring = 'condition=' + condition + '&search=' + search + '&category=' + category + '&brand=' + brand + '&page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
      getProductData(qstring);
  });

  $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
      startDate = '';
      endDate = '';
      qstring = 'condition=' + condition + '&search=' + search + '&category=' + category + '&brand=' + brand + '&page=' + page + '&start_date=' + startDate + '&end_date=' + endDate;
      getProductData(qstring);
  });
            
       function getProductData(qstring) {
            $.ajax({
                type: 'GET',
                url : '{{ URL::to("products?")}}' + qstring,
                dataType: 'json',
            }).done(function (data) {
                $('.product-data').html(data);     
            }).fail(function () {
            });
        }
    });
</script>
@endsection
<style>
    .front-image-td {
        width: 10%;
    }
     .front-image {
        width: 100px;
    }

    @media only screen and (max-width: 768px) {
 .product-details-table {
          display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    }
}

    
</style>