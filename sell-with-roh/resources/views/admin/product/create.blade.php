@extends('admin_template.main') 

@section('title')
    Product - Sell With Roh Roh
@endsection

@section('content')  
<div class="content-header">
      <div class="container-fluid">
          <form class="product-details-form" id="product-details-form" enctype="multipart/form-data">
<div class="row">
  <div class="col-md-12 section-title">
    Personal Details
  </div>
</div>
{{ csrf_field() }}
<div class="row fields">
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Full Name</label>
      <input type="text" class="name form-control" placeholder="Enter Your Name" name="name"/>
      <span class="error name"></span>
    </div>
  </div> 
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Email</label>
      <input type="text" class="email form-control" placeholder="Enter Your Email" name="email"/>
      <span class="error email"></span>
    </div>
  </div> 
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Phone Number</label>
      <input type="number" class="phone-number form-control" placeholder="Enter Your Phone Number" name="phone_number"/>
      <span class="error phone_number"></span>
    </div>
  </div>
</div>

<div class="row fields">
  <div class="col-md-12 section-title">
    Product Details
  </div>
</div>

<div class="row fields">
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Brand</label>
      <select class="form-control brand" name="brand">
        <option value="Aquazzura">Aquazzura</option>
        <option value="Alexander McQueen">Alexander McQueen</option>
        <option value="Alexander Wang">Alexander Wang</option>
        <option value="Balenciaga">Balenciaga</option>
        <option value="Balmain">Balmain</option>
        <option value="Bottega Veneta">Bottega Veneta</option>
        <option value="Burberry">Burberry</option>
        <option value="Bulgari">Bulgari</option>
        <option value="Cartier">Cartier</option>
        <option value="Celine">Celine</option>
        <option value="Chanel">Chanel</option>
        <option value="Chloe">Chloe</option>
        <option value="Christian Louboutin">Christian Louboutin</option>
        <option value="Dior">Dior</option>
        <option value="Dolce & Gabbana">Dolce & Gabbana</option>
        <option value="Emilio Pucci">Emilio Pucci</option>
        <option value="Fendi">Fendi</option>
        <option value="Givenchy">Givenchy</option>
        <option value="Gucci">Gucci</option>
        <option value="Hermes">Hermes</option>
        <option value="Jacquemus">Jacquemus</option>
        <option value="Jimmy Choo">Jimmy Choo</option>
        <option value="Lanvin">Lanvin</option>
        <option value="Louis Vuitton">Louis Vuitton</option>
        <option value="Loewe">Loewe</option>
        <option value="Missoni">Missoni</option>
        <option value="Miu Miu">Miu Miu</option>
        <option value="Moschino">Moschino</option>
        <option value="Mulberry">Mulberry</option>
        <option value="Prada">Prada</option>
        <option value="Salvatore Ferragamo">Salvatore Ferragamo</option>
        <option value="Stella McCartney">Stella McCartney</option>
        <option value="Tom Ford">Tom Ford</option>
        <option value="Valentino">Valentino</option>
        <option value="Versace">Versace</option>
        <option value="Vivienne Westwood">Vivienne Westwood</option>
        <option value="YSL">YSL</option>
      </select>
      <span class="error brand"></span>
    </div>
  </div> 
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Category</label>
      <select class="form-control category" name="category">
        <option value="1">Bag</option>
        <option value="2">Shoes</option>
        <option value="3">Clothing</option>
        <option value="4">Accessories</option>
      </select>
    </div>
  </div> 
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Style/Model code (if known)</label>
      <input type="text" class="model-code form-control" placeholder="Enter Your Model Code" name="model_code" />
    </div>
  </div>
</div>

<div class="row fields">
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Condition</label>
      <select class="form-control condition"  name="condition">
        <option value="1">Pristine - Unused</option>
        <option value="2">Excellent condition - used lightly</option>
        <option value="3">Very good condition – used but well maintained</option>
        <option value="4">Good condition – well used but well maintained</option>
      </select>
    </div>
  </div> 
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Original Tags, Box or Dustbag</label>
      <select class="form-control category" name="box">
        <option selected value="0">No</option>
        <option value="1">Yes</option>
      </select>
    </div>
  </div>
</div>


<div class="row fields">
  <div class="col-md-12">
    <div class="form-group">
      <label class="label-format">Additional Information</label>
      <textarea class="form-control" name="additional_info" rows="4" cols="50"></textarea>
    </div>
  </div> 
</div>

<div class="row fields">
  <div class="col-md-12 section-title">
    Product Images
  </div>
</div>






<div class="row fields">
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Front</label>
      
      <input id="front" class="product-photo" type="file" name="front"/>
      
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Front Side</label>
      
      <input id="front-side" class="product-photo" type="file" name="front_side"/>
      
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Side</label>
      
      <input id="side" class="product-photo" type="file" name="side"/>
     
    </div>
  </div>
</div>

<div class="row fields">
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Back</label>
      <input id="back" class="product-photo" type="file" name="back"/>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Bottom</label>
      <input id="bottom" type="file" name="bottom"/>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label class="label-format">Interior</label>
      <input id="interior" type="file" name="interior"/>
    </div>
  </div>
</div>
<div class="row fields">
  <div class="col-md-12">
    <a href="#" class="submit-product-details"> Submit</a>
  </div>
</div>
          </form>
</div>
</div>

@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.submit-product-details',function(e){
            $.ajax({
                url: "{{ URL::to('product/store') }}",
                type: 'POST',
                dataType: 'json',
                data: new FormData($('#product-details-form')[0]),
                processData: false,
                contentType: false,
            }).done(function (success) {
                console.log(success);
            }).fail(function (error) {
                $('.error').text('');
                if (!jQuery.isEmptyObject(error.responseJSON.errors)) {
                    $.each( error.responseJSON.errors, function( key, value ) {
                        $('.' + key).text(value);
                    });
                }
                
            });   
        });
    });
</script>
@endsection